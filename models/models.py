# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime




class solicitud_memos(models.Model):
    _name = 'memos.memos'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Memos Digitales"


    def action_cancelado(self):
        for rec in self:
            rec.memo_state = 'cancelado'

    def action_borrador(self):
        for rec in self:
            rec.memo_state = 'borrador'

    def action_solicita(self):
        for rec in self:
            rec.memo_state = 'solicita'

    def action_direccion(self):
        for rec in self:
            rec.memo_state = 'direccion'

    def action_validacion(self):
        for rec in self:
            rec.memo_state = 'validacion'

    def action_aprobado(self):
        for rec in self:
            rec.memo_state = 'aprobado'



    memo_director = fields.Many2one('res.partner', string="Director", track_visibility='onchange', index=True, domain="[('x_es_director', '=' , True)]")
    memo_area = fields.Selection([('res_partner','Director')], related='memo_director.x_area', string="Área Dirección", track_visibility='onchange', index=True, readonly=True)
    memo_establecimiento_salud = fields.Selection([('res_partner','Director')], related='memo_director.x_establecimiento_salud', string="Establecimiento Salud", track_visibility='onchange', index=True, readonly=True)
    memo_establecimiento_educacion = fields.Selection([('res_partner','Director')], related='memo_director.x_establecimiento_educacion', string="Establecimiento Educación", track_visibility='onchange', index=True, readonly=True)

    memo_ti = fields.Boolean(string="Concierne TI", default=False, track_visibility='onchange', index=True)
    memo_infra = fields.Boolean(string="Concierne Infraestructura", default=False, track_visibility='onchange', index=True)

    memo_product_ids = fields.One2many('memos.products','memo_id', string="Productos",track_visibility='onchange', index=True)



    memo_date_request = fields.Date(string='Fecha Solicitud', default=datetime.today(), readonly=True)
    memo_state = fields.Selection([('solicita', 'Solicita'),('direccion', 'Dirección'),('validacion', 'Validación'),('aprobado', 'Aprobado'),('cancelado', 'Cancelado'), ('borrador', 'Borrador')],string="Estados", readonly=True, index=True, copy=False, default='borrador', track_visibility='onchange' )
    memo_priority = fields.Selection([('0', 'Muy Baja'),('1', 'Baja'),('2', 'Normal'),('3', 'Alta')],string="Prioridad", index=True, default='0', track_visibility='onchange' )



    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)



class memo_products(models.Model):
    _name = 'memos.products'

    _description = "Memos Product"


    name_product = fields.Char(string="Nombre Producto o Servicio", track_visibility='onchange', index=True)
    description_product = fields.Char(string="Descripción de Producto o Servicio", track_visibility='onchange', index=True)
    qty_product = fields.Integer(string="Cantidad", track_visibility='onchange', index=True)
    memo_id = fields.Many2one('memos.memos', 'memo_director')
    url_product = fields.Char(string="URL", index=True)