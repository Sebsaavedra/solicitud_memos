# -*- coding: utf-8 -*-
from odoo import models, fields, api


#class casos_seguridad(models.Model):
class memo_products(models.Model):
    _name = 'memos.products'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Memos Product"


    name_product = fields.Char(string="Nombre Producto o Servicio", track_visibility='onchange', index=True)
    description_product = fields.Char(string="Descripción de Producto o Servicio", track_visibility='onchange', index=True)
    qty_product = fields.Integer(string="Cantidad", track_visibility='onchange', index=True)
    memo_id = fields.Many2one('memos.memos', 'id')
    url_product = fields.Char(string="URL", index=True)




